# Italian translation of sensible-utils man page.
# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the sensible-utils package.
# Beatrice Torracca <beatricet@libero.it>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: sensible-utils\n"
"POT-Creation-Date: 2017-08-11 09:11+0200\n"
"PO-Revision-Date: 2012-05-12 18:33+0200\n"
"Last-Translator: Beatrice Torracca <beatricet@libero.it>\n"
"Language-Team: Italian <debian-l10n-italian@lists.debian.org>\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Virtaal 0.7.1\n"

#. type: TH
#: sensible-editor.man
#, no-wrap
msgid "SENSIBLE-EDITOR"
msgstr "SENSIBLE-EDITOR"

#. type: TH
#: sensible-editor.man
#, no-wrap
msgid "14 Nov 2010"
msgstr "14 Novembre 2010"

#. type: TH
#: sensible-editor.man
#, no-wrap
msgid "Debian"
msgstr "Debian"

#. type: SH
#: sensible-editor.man
#, no-wrap
msgid "NAME"
msgstr "NOME"

#. type: Plain text
#: sensible-editor.man
msgid ""
"sensible-editor, sensible-pager, sensible-browser - sensible editing, "
"paging, and web browsing"
msgstr ""
"sensible-editor, sensible-pager, sensible-browser - modifica di testi, "
"paginazione e navigazione web ragionevoli"

#. type: SH
#: sensible-editor.man
#, no-wrap
msgid "SYNOPSIS"
msgstr "SINTASSI"

#. type: Plain text
#: sensible-editor.man
msgid "B<sensible-editor> [OPTIONS...]"
msgstr "B<sensible-editor> [OPZIONI...]"

#. type: Plain text
#: sensible-editor.man
msgid "B<sensible-pager> [OPTIONS...]"
msgstr "B<sensible-pager> [OPZIONI...]"

#. type: Plain text
#: sensible-editor.man
msgid "B<sensible-browser> url"
msgstr "B<sensible-browser> URL"

#. type: SH
#: sensible-editor.man
#, no-wrap
msgid "DESCRIPTION"
msgstr "DESCRIZIONE"

#. type: Plain text
#: sensible-editor.man
msgid ""
"B<sensible-editor>, B<sensible-pager> and B<sensible-browser> make sensible "
"decisions on which editor, pager, and web browser to call, respectively.  "
"Programs in Debian can use these scripts as their default editor, pager, or "
"web browser or emulate their behavior."
msgstr ""
"B<sensible-editor>, B<sensible-pager> e B<sensible-browser> prendono "
"decisioni sensate rispettivamente su quale editor, paginatore e browser web "
"richiamare. I programmi in Debian possono usare questi script come loro "
"editor, paginatore o navigatore web predefinito oppure emulare il loro "
"comportamento."

#. type: SH
#: sensible-editor.man
#, no-wrap
msgid "SEE ALSO"
msgstr "VEDERE ANCHE"

#. type: Plain text
#: sensible-editor.man
#, fuzzy
#| msgid ""
#| "Documentation of the EDITOR, VISUAL, PAGER, and BROWSER variables in "
#| "B<environ>(7)"
msgid ""
"Documentation of the EDITOR, VISUAL and PAGER variables in B<environ>(7)  "
"and B<select-editor>(1)  for changing a user's default editor"
msgstr ""
"La documentazione delle variabili EDITOR, VISUAL, PAGER e BROWSER in "
"B<environ>(7)."

#. type: SH
#: sensible-editor.man
#, no-wrap
msgid "STANDARD"
msgstr ""

#. type: Plain text
#: sensible-editor.man
msgid ""
"Documentation of behavior of sensible-utils under a debian system is "
"available under section 11.4 of debian-policy usually installed under /usr/"
"share/doc/debian-policy (you might need to install debian-policy)"
msgstr ""
